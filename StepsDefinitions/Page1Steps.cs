﻿using System;
using Baseclass.Contrib.SpecFlow.Selenium.NUnit.Bindings;
using OpenQA.Selenium;
using TechTalk.SpecFlow;
using VicRoads_UPV.PageObjects;

namespace VicRoads_UPV.StepsDefinitions
{
    [Binding]
    public class Page1Steps
    {
        IWebDriver currentDriver = null;


        String test_url = "https://www.vicroads.vic.gov.au/registration/limited-use-permits/unregistered-vehicle-permits/get-an-unregistered-vehicle-permit";

        Page1Objects page = new Page1Objects();
        [Given(@"I have navigated to UPV page")]
        public void GivenIAmOnVicRoadUPVPageOne()
        {
            Browser.Current.Navigate().GoToUrl(test_url);
            currentDriver = Browser.Current;
        }
       
        [Given(@"select (.*) and (.*) and (.*) and (.*) on the page")]
        public void GivenFillVehicletypeVehiclesubtypeAddressPermitdurationOnThePage(string vehicletype, String vehiclesubtype, string address, String permitduration)
        {
            page.vehicle_select_list(vehicletype);
            page.vehicle_sub_select_list(vehiclesubtype);
            page.EnterData(address);
            page.permit_select_list(permitduration);
        }
        
        [When(@"I click the next button")]
        public void WhenIClickTheNextButton()
        {
            PropertiesCollection.currentPage = PropertiesCollection.currentPage.As().ClickNext();
        }
        
        [Then(@"It should navigate page two")]
        public void ThenItShouldNavigatePageTwo()
        {
            PropertiesCollection.currentPage.As().verify_progress_title();
        }
    }
}
