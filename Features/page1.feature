﻿Feature: page1
	Go to vic roads UPV page1
	Enter all the data in the page1 form
	Click on the next button
	vic roads UPV page2 is displayed

@page1
Scenario Outline: Enter the data in page1 and click next should navigate page2
	Given I have navigated to UPV page
	And select <vehicletype> and <vehiclesubtype> and <address> and <permitduration> on the page
	When I click the next button
	Then It should navigate page two
	Examples:
| vehicletype          | vehiclesubtype		| address										 | permitduration |
| PassengerVehicle	   | Sedan				| Unit 7 11 Sample Street, Broadmeadows VIC 3047 | 1          |
| GoodsCarryingVehicle | TwoTonnesOrLess    | 113 Exhibition St, Melbourne VIC 3000          | 2          |