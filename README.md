# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* SpecFlow Selenium Framework-The project is to Navigate from Page 1 of UVP page to Page 2 of UPV page and to verify �Step 2 of 7: Select permit type� is displayed

### Uses:### 

    * SpecFlow (BDD)
    *  Selenium (WebDriver)
    *  NUnit 2.x
    *  utilises Page Object Model pattern

### Tool stack:### 
 * IDE-Visual Studio 2019
 * BDD Interpreter-SpecFlow
 * Automation API- 	WebDriver
 * Testing Framework-NUnit

### Background reading:### 
1. Install Visual Studio (Enterprise 2019)
2. Use NuGet (Project > Manage NuGet packages) to install Specflow, Nunit and Selenium:
	Selenium
	Specflow.Assist.Dynamics
	specflow
	Baseclass.Contrib.SpecFlow.Selenium.NUnit
 3. Right click on the chromedriver.exe and select Properties to ensure that chromedriver.exe is always in the folder of the running assembly so it can be used.
 4. Copy the project files.
 5. Build the project.