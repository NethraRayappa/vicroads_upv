﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Baseclass.Contrib.SpecFlow.Selenium.NUnit.Bindings;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using VicRoads_UPV.StepsDefinitions;

namespace VicRoads_UPV.PageObjects
{
    class Page1Objects : BasePage
    {
        
        public Page1Objects()
        {
            PageFactory.InitElements(Browser.Current, this);
        }
        
        [FindsBy(How = How.Name, Using = "ph_pagebody_0$phthreecolumnmaincontent_0$panel$VehicleType$DDList")]
        public IWebElement vehicletypeselect;
        public void vehicle_select_list(String vehicletype)
        {
            SelectElement vehicletypeDropdown = new SelectElement(vehicletypeselect);
            vehicletypeDropdown.SelectByValue(vehicletype);
        }

        [FindsBy(How = How.Name, Using = "ph_pagebody_0$phthreecolumnmaincontent_0$panel$PassengerVehicleSubType$DDList")]
        public IWebElement vehiclesubtypeselect;
        public void vehicle_sub_select_list(String vehiclesubtype)
        {
            SelectElement vehiclesubtypeDropdown = new SelectElement(vehicletypeselect);
            vehiclesubtypeDropdown.SelectByValue(vehiclesubtype);
        }
        
        [FindsBy(How = How.Name, Using = "ph_pagebody_0$phthreecolumnmaincontent_0$panel$AddressLine$SingleLine_CtrlHolderDivShown")]
        public IWebElement addressfeild;

        [FindsBy(How = How.Name, Using = "ph_pagebody_0$phthreecolumnmaincontent_0$panel$PermitDuration$DDList")]
        public IWebElement permitdurationselect;
        public void permit_select_list(String permitduration)
        {
            SelectElement permitDropdown = new SelectElement(vehicletypeselect);
            permitDropdown.SelectByValue(permitduration);
        }

        [FindsBy(How = How.Name, Using = "ph_pagebody_0$phthreecolumnmaincontent_0$panel$btnNext")]
        public IWebElement btnNext;

        
        public void EnterData(string address)
        {
            addressfeild.SendKeys(address);
         }

        public Page2Objects ClickNext()
        {
            btnNext.Submit();
            return new Page2Objects();
        }



    }
}
