﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Baseclass.Contrib.SpecFlow.Selenium.NUnit.Bindings;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using VicRoads_UPV.StepsDefinitions;

namespace VicRoads_UPV.PageObjects
{
    class Page2Objects : BasePage
    {
        public Page2Objects()
        {
            PageFactory.InitElements(Browser.Current, this);
        }

        [FindsBy(How = How.XPath, Using = "/html/body/form/div[3]/div/div/div[3]/div[2]/div[2]/div/div/div/p")]
        public IWebElement page2title;

        public void verify_progress_title()
        {
            if (page2title.Text.Contains("Step 2 of 7 : Select permit type"))
            {
                Console.WriteLine("Test Verification is passed");
            }
            else
                Console.WriteLine("Test Verification is Failed");
        }


        //Nunit assertion
        //Assert.IsTrue(page2title.Text.Contains("Step 2 of 7 : Select permit type"));
         //Console.WriteLine("UPV verification Test Passed");
    }
}
